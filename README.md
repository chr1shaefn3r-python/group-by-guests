# group-by-guests 

## Setup

Python3 and pip
```bash
$ sudo apt-get install python3 python3-pip
```

pipenv
```bash
$ sudo pip install --user pipenv
```

Cmake (to build dlib, dependency of face\_recognition)
```bash
$ sudo apt-get install build-essential cmake
```

## Useage
```bash
$ bash face-cluster.sh my-images/
```
