# USAGE
# python cluster_faces.py --encodings encodings.pickle

# import the necessary packages
from sklearn.cluster import DBSCAN
from imutils import build_montages
import numpy as np
import argparse
import pickle
import cv2
import os
import shutil

# construct the argument parser and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--dataset", required=True,
                help="path to input directory of faces + images")
ap.add_argument("-e", "--encodings", required=True,
                help="path to serialized db of facial encodings")
ap.add_argument("-j", "--jobs", type=int, default=-1,
                help="# of parallel jobs to run (-1 will use all CPUs)")
args = vars(ap.parse_args())

# load the serialized face encodings + bounding box locations from
# disk, then extract the set of encodings to so we can cluster on
# them
print("[INFO] loading encodings...")
data = pickle.loads(open(args["encodings"], "rb").read())
data = np.array(data)
encodings = [d["encoding"] for d in data]

# cluster the embeddings
print("[INFO] clustering...")
clt = DBSCAN(metric="euclidean", n_jobs=args["jobs"])
clt.fit(encodings)

# determine the total number of unique faces found in the dataset
labelIDs = np.unique(clt.labels_)
numUniqueFaces = len(np.where(labelIDs > -1)[0])
print("[INFO] # unique faces: {}".format(numUniqueFaces))

result_folder = os.path.join(args["dataset"], "proPerson")
os.makedirs(result_folder, exist_ok=True)
print("[INFO] created result folder: {}".format(result_folder))


def createMontage(idxs, data):
    # initialize the list of faces to include in the montage
    faces = []

    # loop over the sampled indexes
    for i in idxs:
        # load the input image and extract the face ROI
        image = cv2.imread(data[i]["imagePath"])
        (top, right, bottom, left) = data[i]["loc"]
        face = image[top:bottom, left:right]

        # force resize the face ROI to 96x96 and then add it to the
        # faces montage list
        face = cv2.resize(face, (96, 96))
        faces.append(face)

    # create a montage using 96x96 "tiles" with 5 rows and 5 columns
    return build_montages(faces, (96, 96), (5, 5))[0]


# loop over the unique face integers
for labelID in labelIDs:
    # find all indexes into the `data` array that belong to the
    # current label ID, then randomly sample a maximum of 25 indexes
    # from the set
    print("[INFO] faces for face ID: {}".format(labelID))
    idxs = np.where(clt.labels_ == labelID)[0]
    idxs_randoms = np.random.choice(idxs, size=min(25, len(idxs)),
                                    replace=False)

    montage = createMontage(idxs_randoms, data)

    folder = "" if labelID == -1 else "Gast{}".format(labelID)
    montage_folder = os.path.join(result_folder, folder)
    os.makedirs(montage_folder, exist_ok=True)
    print("[INFO] created montage folder: {}".format(montage_folder))
    montage_filename = "Unknowns.jpg" if labelID == - \
        1 else "Gast{}Montage.jpg".format(labelID)
    montage_path = os.path.join(montage_folder, montage_filename)
    print("[INFO] will write montage to: {}".format(montage_path))
    # show the output montage
    title = "Face ID #{}".format(labelID)
    title = "Unknown Faces" if labelID == -1 else title
    cv2.imwrite(montage_path, montage)
    for i in idxs:
        # Identical to copy() except that copy2() also attempts to preserve file metadata.
        shutil.copy2(data[i]["imagePath"], montage_folder)
