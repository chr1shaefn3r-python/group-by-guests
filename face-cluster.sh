#!/bin/bash

set -e; # Exit immediately if a simple command exits with a non-zero status (-o errexit)
set -x; # Print a trace of simple commands and their arguments (-o xtrace)

pipenv run python encode_faces.py --dataset "$1" --encodings tmp.pickle
pipenv run python cluster_faces.py --dataset "$1" --encodings tmp.pickle
eog "$1"/proPerson/**/Gast*Montage.jpg
